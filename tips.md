---
title: General Tips
permalink: /tips/
layout: default
---

Copy a file to the phone via USB
--------------------------------

To copy a file from your host-system to the phone:

    $ scp /path/to/file phablet@10.15.19.82:/home/phablet

The password for the “phablet” user is “1234”.

Remote Connecting via USB
-------------------------

If your phone is connected via usb and running Plasma Mobile, it can be
remotely controlled from the attached computer via command line using
ssh:

    $ ssh phablet@10.15.19.82

The password is “1234”.

To easily establish a wifi connection, execute:

    $ nmcli dev wifi con "ssid" password "password"

To resize the root partition on the phone, First reboot to recovery
and then from adb shell, run following.

    # e2fsck -yf /data/rootfs.img
    # resize2fs -f /data/rootfs.img 1024000

This will double the size of the rootfs.

Notes:
-----

-   Use sudo to access the root user on phone, password for phablet user is 1234
