---
title:      "Plasma Mobile sprint in Berlin"
created_at: 2019-03-11 10:30:00 UTC+1
author:     Plasma Mobile team
layout:     post
---

During the week of 4 to 10 February, the KDE Plasma Mobile team held the first ever Plasma Mobile sprint in Berlin.

On the first day, we collected important tasks, planned our work and discussed future releases of the project.

In the following days, we worked on the following tasks:

![Shell UI](/img/screenshots/screenshot_20190215_01.png){: .blog-post-image-right}

# User interface

Ilya Bizyaev  visually refreshed the Plasma Mobile shell and brought it nearer to the mockups. Meanwhile, Marco Martin refactored and simplified the codebase of the top sliding panel, making the UI code simpler and more maintainable.

# Documentation

Dimitris Kardarakos improved the documentation to make the development environment setup and the application development easier for everyone. Thanks to [his work](https://invent.kde.org/websites/docs-plasma-mobile-org/commit/6386a5e34ed36c67f1092108b62933be4a1645dc), we now have a [Kirigami tutorial](https://docs.plasma-mobile.org/AppDevelopment.html#create-a-kirigami-application). Moreover, the instructions to build QEMU and Virgil 3D from the source code have been substituted with the installation of just a single snap package. He also explored new ways we could leverage Flatpak and update the website with the Debian based images.

Ilya Bizyaev helped make the new documentation resources more discoverable by cleaning up old wiki pages and setting redirects where appropriate.

# Infrastructure

During the sanity checks of the Plasma Mobile documentation, we found that building Flatpak for the phone was failing in KDE Neon User Edition. Since the root cause lies in the qemu-user-static and binfmt configuration in bionic, Harald Sitter provided a [workaround](https://packaging.neon.kde.org/neon/settings.git/commit/?h=Neon/release&id=e2a572c4f767aea8c3fcce08638f57e2230e8722) until the issue gets solved upstream.

![Kaidan and the new emoji picker](/img/screenshots/screenshot_20190209_5.png){: .blog-post-image-right}

# Applications

Simon Schmeisser worked on our mobile Angelfish web browser which now [makes more use of Kirigami](https://commits.kde.org/plasma-angelfish/335be74ee41250284ebd2e1c0cd12e386cb42515), [displays favicons](https://commits.kde.org/plasma-angelfish/a76bd5ac1318f32e711a506a0af26949e7fe55d0), and provides [autocompletion of search queries](https://commits.kde.org/plasma-angelfish/0b82ae2c75023e7c12aad02fcb2bbfde55dd8469). We are planning to give Angelfish a more Kirigami-streamlined look once Marco Martin's patch for resizing windows on keyboard input is accepted. Related to this, Jonah Brüchert added settings for configuring a search engine and a homepage.

Linus worked on Kaidan, an XMPP Messaging client for Plasma Mobile and many other platforms. Kaidan now [provides a download manager](https://git.kaidan.im/kaidan/kaidan/commit/4078f1dbe13e29a19f156b961c66c5217fdda972) to download and cache files instead of downloading them again each time it starts. Jonah's merge request to add an emoji picker to Kaidan was also accepted. You can expect more features and a rewritten database backend soon.

Marco Matrin fixed and improved many areas of Kirigami, enabling some of our changes in Angelfish we mentioned above. One of the improvements makes it possible to [leave Kirigami layers on a mobile device](https://commits.kde.org/kirigami/dfe1610ef5825f14bba78855b57130810e76a476) without requiring the app developer to add a close button, another is that Kirigami also got a new API to customize the application top bar headers. A lot of work has been done to refactor its signature column-based navigation UI which will allow for many new features.

Nicolas Fella worked on improving KDE Connect on Plasma Mobile. Most of the work went into integrating the settings from the desktop side in the mobile app. We also verified that the SMS and telephony stack of the Nexus 5X is functional and discussed ideas for an appropriate UI.

Camilo Higuita has been working on the MauiKit framework and the Maui set of apps for almost a year now, and some of those apps are going to be shipped with Plasma Mobile by default. [His work](https://medium.com/@temisclopeolimac/maui-plasma-mobile-sprint-2019-c20031700b3b) mostly consisted of bringing those apps up to date, adding missing features and fixing bugs along the way. He also worked on getting acquainted with the Plasma Mobile shell, its plasmoids, their architecture and structure since he plans on eventually hacking on the shell. All the Maui apps (Index, Vvave, Buho and Nota) are now using a better modeling system that allows for better control over the content, give users a cleaner  interface and has gained some features along the way, like content sorting, file searching, better editor control and more.

![Kaidan being recommended in Discover](/img/screenshots/screenshot_20190209_2.png){: .blog-post-image-right}

# Packaging and Software management

Jonah Brüchert created a [patch for Discover that fixes icons being incorrectly displayed](https://commits.kde.org/discover/502b963bcc96e5e9abb257e2cc90d3fea2212996) on the update page and made all of our improvements and new applications available in the Debian repository. Continuing with Discover, Aleix Pol also [fixed the issue of non-mobile-friendly applications being recommended in Discover on Plasma Mobile](https://commits.kde.org/discover/b41f8448dc229b899c6e7e811969ac9f861b9497). Aleix also made many new mobile-friendly applications available in the KDE Flatpak repository.

Bhushan Shah worked on further improving our KDE Neon-based images. They should now be up to date with the latest changes including patches for better scaling of the Plasma Shell and applications under [hwcomposer backend](https://commits.kde.org/kwin/3dc22d7d8882b1035abf1140e92778611c835bfb) from David Edmundson. Meanwhile, Bart Ribbers made more Plasma Mobile apps and updates available in postmarketOS, so you can now, for example, install and use Kaidan and Angelfish on it.

# Devices and Hardware

In addition to software work, the Plasma Mobile team also worked on various hardware projects. Before the sprint, the KDE Community attended FOSDEM, where [we demonstrated Plasma Mobile running on the RISC-V hardware](https://twitter.com/atishp04/status/1088888665663664130), thanks to the work carried out by Alistair Francis from Western Digital.

![Plasma Mobile running on RISC-V](/img/plamo-riscv.jpg){: .blog-post-image-centered}

During the sprint, we were joined by Dorota Czaplejewicz from [Purism](https://puri.sm/) who works for the [Librem 5 project](https://puri.sm/products/librem-5/). Purism has provided Librem 5 developer kits to Plasma Mobile developers, and with help from Dorota, we were able to bring Plasma Mobile up on the kits.

![Plasma Mobile running on Purism Librem 5 devkit](/img/plamo-librem5-devkit.jpg){: .blog-post-image-centered}

Talking of which, during FOSDEM and the sprint, Bart Ribbers also worked on bringing up Plasma Mobile on the [Pinephone devkit, Project Anakin](http://wiki.pine64.org/index.php/Project_Anakin).

![Plasma Mobile running on Project Anakin](/img/plamo-anakin.jpg){: .blog-post-image-centered}

We are excited about open hardware projects and would like to thank the companies working on these devices.

# Community feedback

We actively collaborated with community members during the sprint. To make sure everyone's opinions were considered, we carried out an [AMA (Ask Me Anything) session on Reddit](https://redd.it/anspo5). Bhushan also [live-tooted](https://fosstodon.org/@bshah/101539016596663578) about our work.

![Group photo](/img/plasma-mobile-sprint.jpg){: .blog-post-image-centered}

Our first sprint was a great experience, and we are happy about growing so significantly as a project since Akademy.  Marco invited us to a united Plasma Sprint and we are looking forward to continuing our work there! If you want to join and help us with this project then [our todo board](https://phabricator.kde.org/tag/plasma%3A_mobile/) is a good place to get started.

![Plasma Sprint todo items](/img/plamo-sprint-todo.jpg){: .blog-post-image-centered}

And, who knows? Maybe we will get a chance to welcome you as a new developer at our next sprint :)
