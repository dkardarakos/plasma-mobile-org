---
title:      "Plasma Mobile update: May-August 2020"
created_at: 2020-08-25 08:30:00 UTC
author:     Plasma Mobile team
layout:     post
---

Once again it's been a while since the last Plasma Mobile update was published, but as always that doesn't mean nothing happened.

It's almost hard to believe, but in the meantime we gained several completely new applications! Meet our new clock, weather, calculator and color contrast checking apps.

{% section {"app_name": "KClock", "app_img": "/img/kclock_alarms.png", "app_img_shadow": "true", "app_img_alt": "KClock, the new clock application"} %}

Development has been ongoing for several months and basic functionality is working now. The first stable releases of these will come after Plasma 5.20.

As you would expect from any self-respecting clock app, it allows you to:

* Show a customizable list of clocks from different cities and timezones
* Use it as a stopwatch, including lap functionality
* Set multiple timers
* Set alarms with customizable ringtones, repeating behaviour, and snoozing lengths

It also features a plasmoid that you can add to your home screen.

{% endsection %}

{% section {"app_name": "KWeather", "app_img": "/img/kweather.png", "app_img_shadow": "true", "direction": "right", "app_img_alt": "KWeather, the new weather applcation, showing the current weather as \"Cloudy\" and a daily and hourly forecast"} %}

The first stable release with basic functionality was tagged a few weeks ago!

Features include:

* The ability to add cities by name
* The ability to add the current location
* The ability to choose between two weather backends, the ([Norwegian Meterological Institute](https://api.met.no/) and [OpenWeatherMap](https://openweathermap.org/))
* A day-based view, showing the condition and temperature highs and lows for each day
* An hour-based view, showing temperature, condition, wind speed and precipitation for each hour
* Detailed weather cards, showing more detailed weather information about the selected day
* The ability to switch the temperature and speed units
* A plasmoid for  home screen

<figure>
  <img src="/img/post-2020-08-kweather-plasmoid.png" class="shadow-true blog-post-image-small" alt="" />
  <figcaption>KWeather plasmoid</figcaption>
</figure>

In the future, more work will be done on making it more tablet/desktop friendly and adding more plasmoids for the home screen.

{% endsection %}


{% section {"app_name": "Kalk", "app_img": "/img/Kalk_Screenshot_20200818_144710.png", "app_img_alt": "Kalk, calculator"} %}

Another new app worth mentioning is Kalk: a calculator with unit and currency conversion functionality.
With a math expression parser written with GNU Bison and flex, Kalk is still in early alpha stage.

Features:

* Basic math
* Some advanced math operations
* Calculation history
* Unit conversion
* Currency conversion

{% endsection %}

<!-- possibly more to go here -->

## Angelfish

Angelfish now uses the same scrollbars that other apps use, replacing the previously used scrollbars provided by Chromium.
Confirmation dialogs now feature a cancel button, because the effect of closing the sheet by tapping outside it was too unclear.
Opening links from other applications now opens a new tab in the existing instance instead of launching a new instance.

Besides the user-facing improvements a lot of internal cleanup has been done. Angelfish now makes better use of KConfig's abilities and avoids unnecessary page reloads and the internal webapp runtime now shares more code with the main browser.

{% section {"app_name": "Kontrast", "app_img": "/img/kontrast_mobile.png", "app_img_alt": "Kontrast, screenshot oof the main view", "app_img_shadow": "true"} %}

Kontrast is a new application made by Carl aimed at developers and designers. It allows experimenting with different colors and their contrast ratio, which is important for designing accessible user interfaces. It also allows you to generate and save color sets with good contrast.

{% endsection %}

{% section {"app_name": "Plasma Samegame", "app_img": "/img/post-2020-08-samegame.png", "app_img_alt": "Plasma Samegame", "direction": "right", "app_img_shadow": "true"} %}

Carl also revived one of the oldest Plasma Mobile demo applications, ported it to Kirigami and added shiny high-resolution textures.

{% endsection %}

{% section {"app_name": "Calindori", "app_img": "/img/todo_1.png", "app_img_alt": "Calindori Task", "direction": "left", "app_img_shadow": "true"} %}

- Users can now change the start date of a task, set a due date and add an alarm based on its start date.
- When a new event is created, a reminder is added by default. Users can opt out or add a custom alarm time in the settings page.
- When the month name is not accompanied with a day number, the standalone name of the month is displayed.
- When an event or task starts and ends within the same hour, we ensure that it is always displayed.
- When clicking on "Today" on the main action toolbar, the selected day is set to the current date. When navigating to the previous or next month, it is set to the 1st of that month.
- On the week view, the current day is set as the selected one, while on the day view the current hour is selected by default.
- For new events, the start time is set to the next hour. Nevertheless, the user may change the default event duration in the application settings.

{% endsection %}

{% section {"app_img": "/img/time_picker_1.png" , "direction": "right", "app_img_alt": "Calindori Time Picker", "app_img_shadow": "true" } %}

- On the day and week view, start and end time (when applicable) is displayed for each incidence. Moreover, events are displayed ordered by their start date.
- The time picker AM/PM selector has been changed to radio buttons in order to be more consistent with the rest of the KDE applications.
- The vertical date swipe mechanism has been simplified, yielding better performance.
- When using the application on the desktop, the global drawer is turned into a sidebar.
- When using the application on a wide screen, the incidences of each week/day are displayed in a row. On mobile devices, the incidences of each week/day are displayed in a column view.
- The page that shows the events scheduled on a specific date alongside the calendar-month page is loaded by default on non-mobile devices. On mobile, only the calendar-month page is loaded.

{% endsection %}

##  File Dialogs

A mobile-friendly file dialog has finally landed in xdg-desktop-portal-kde, making it available for apps using xdg-portals, like Flatpak'd apps. How to support apps not using portals is still being discussed.

## Alligator

Lots of smaller improvements have gone into Alligator recently, mostly improving the usability and polish of our feed reader. One significant improvement, which was surprisingly hard to achieve, was making sure that images would be limited to the width of the application. The work on this is still ongoing, since it requires modifying the feed's content in specific ways, which have yet to be figured out for some rss feed engines (looking at you, wordpress).

## Kongress

Akademy 2020 has been added to the Kongress conference companion application. So, this year you will be able to organize your Akademy schedule on a mobile device running Plasma using a Kirigami application!

{% section {"app_name": "Voice recorder", "app_img": "/img/krecorder.png", "app_img_alt": "Audio input visualization of the voice recorder app", "direction": "left", "app_img_shadow": "true"} %}

Devin implemented a new audio visualization, which is displayed while recording and while playing existing recordings using the new integrated media player.

The Settings page was reworked to only display the settings relevant to most users by default, while providing more advanced settings if needed.

{% endsection %}

## Settings

While many desktop specifics settings modules get ported to Kirigami and Plasma Mobile, some new mobile-specific settings modules are also developed. This time a cellular network module was added by Dimitris to get information about the SIM Card, modem, and carrier registration.

Scrolling in the "info" settings module was fixed by Jonah.

## KDE Connect

The plasma mobile version of KDE connect was also improved and it is now easier to use, thanks to multiple small visual and usability fixes. For example, it is now possible to refresh the list of devices by pulling it down, and it is no longer possible to accidentally switch page when using the remote touchpad plugin.

{% section {"app_name": "Plasma Shell", "app_img": "/img/post-2020-08-plasmashell.png", "app_img_alt": "Plasma Shell", "direction": "left", "app_img_shadow": "true"} %}

Thanks to Marco's work, the shell user interface has gained many improvements. Already running apps are now activated instead of starting a new instance when tapping them on the homescreen; the app splashscreen and the panels are now colored according to the app's icon; and the task switcher has gained many usability improvements.

Thanks to Aleix's work, the task switcher now shows thumbnails of the running application and the search field also gained a new look.

Devin has worked on refining the lock screen. It now allows swiping up to reveal the PIN entry field and darkens the background to make the controls more legible.
{% endsection %}

## Neon on Halium

Most developers have received or bought Pinephones by now and are no longer focusing on the Nexus 5X hardware. Therefore focus of the "core team" has shifted and no new Neon image for Halium based devices (liberated Android phones) has been published since February. Simon started an effort to port the image generation to use debos just like it's being used for the Pinephone images. There are some EGL/library loading issues remaining however and he may not have time to fix them soon. Please join the Matrix channel if this is your thing!

On a brighter note, Erfan managed to write a kwin backend for Halium 9, theoretically allowing Plasma Mobile to run on much newer Android hardware.

## Events

Bhushan Shah will be giving a talk at the Linux Plumbers Conference 2020 - Application Ecosystem MC about [Plasma on Mobile devices](https://linuxplumbersconf.org/event/7/contributions/835/) this Friday. This event will be live-streamed on YouTube. We are also organizing the BoF session on Plasma Mobile during the KDE Akademy.
