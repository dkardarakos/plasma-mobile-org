---
title:      "Plasma Mobile: weekly update: part 4"
created_at: 2019-10-25 14:00:00 UTC+2
author:     Plasma Mobile team
layout:     post
---

The Plasma Mobile team is happy to present the fourth weekly blogpost. This week's update features various shell user-interface improvements, bug fixes as well as polishing of various applications.

## Shell User Interface

Plasma Mobile now uses the same notification code that is used on the desktop, which has received some slight adjustments when running on a phone (Marco Martin).

The dialer now uses the SearchField component from Kirigami, giving it the same look and feel as in similar places (Jonah Brüchert).

The icons in the top drawer are consistently monochrome now (Nicolas Fella).

![Notifications](/img/screenshots/notifications_w4.png){: .blog-post-image-small}

When plugging in a SIM card the SIM pin page now appears correctly again (Bhushan Shah).

## Settings Application:

The WiFi settings have received an overhaul. Distinguishing the active connection is now easier due to connections being grouped by their state. Furthermore, it's now possible to forget a connection that has been used before (Nicolas Fella).

![WiFi config module](/img/screenshots/kcmwifi_w4.png){: .blog-post-image-small}

## Calindori:

In our calendar app navigating to the previous/next month can now be done by swiping up/down.

The calendar selection in the left drawer now clearly indicates the currently active calendar (Dimitris Kardarakos).

![Calindori calendar selection](/img/screenshots/calindori_w4.png){: .blog-post-image-small}

## Angelfish:

Our browser has received fixes for visual glitches in the URL completion. It is now possible to request the desktop version of a site if desired. The history is now sorted by the last time a site was accessed (Jonah Brüchert).

![Angelfish web browser side drawer](/img/screenshots/angelfish_w4.png){: .blog-post-image-small}

## KTrip:

Our public transport assistant has received some visual improvements. It now also shows more types of transport when using the Deutsche Bahn backend (Nicolas Fella).

![A connection view in KTrip](/img/screenshots/ktrip_w4.png){: .blog-post-image-small}

## Downstream:

Plasma 5.17 is now available in postmarketOS, now also including Plasma Phonebook (Bart Ribbers).

## Want to be part of it?

Next time your name could be here! To find out the right task for you, from promotion to core system development, check out [Find your way in Plasma Mobile](https://www.plasma-mobile.org/findyourway). We are also always happy to welcome new contributors on our [public channels](https://www.plasma-mobile.org/join). See you there!
