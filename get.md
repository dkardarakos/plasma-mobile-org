---
title: Get Plasma Mobile
permalink: /get/
layout: default
---

Distributions offering Plasma Mobile
====================================

**Mobile**
----------

Neon based reference rootfs (recommended)
-----------------------------------------

![](/img/neon.svg){:width="100"}

"Official" image, based on KDE Neon. Targeted at newer devices (with 1GB+ RAM).
KDE Neon itself is based upon Ubuntu 18.04 (bionic). This image is based on the dev-unstable branch of KDE Neon, and always ships the latest versions of KDE frameworks, kwin and Plasma Mobile. On devices shipped with Android, it uses Halium as hardware adaption layer.

Download:
[<span class="fa fa-download" /> halium generic](https://images.plasma-mobile.org/rootfs/)
[<span class="fa fa-download" /> halium caf](https://images.plasma-mobile.org/caf-rootfs/)
[<span class="fa fa-download" /> Pine Phone](https://images.plasma-mobile.org/pinephone/)

**[<span class="fa fa-angle-right" /> Installation Guide for Nexus 5X](https://docs.plasma-mobile.org/HaliumInstallation.html)**


postmarketOS
------------

![](/img/pmOS.svg){:width="100"}

PostmarketOS (pmOS), is a touch-optimized, pre-configured Alpine Linux that can be installed on smartphones and other mobile devices. The project is at very early stages of development and is not usable for most users yet.

**[<span class="fa fa-angle-right" /> Learn more](https://postmarketos.org)**


**Desktop Devices**
-----------

Neon based amd64 ISO image
---------------------------

![](/img/neon.svg){:width="100"}

This ISO image is using the same packages as the Neon based reference rootfs, just compiled for amd64. It can be tested on non-android intel tablets, PCs and virtual machines.

**[<span class="fa fa-angle-right" /> Download](https://files.kde.org/neon/images/mobile/current/)**
