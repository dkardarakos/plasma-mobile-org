---
title: Installation
permalink: /neon-arch-reference-rootfs/
layout: default
---

<meta http-equiv="refresh" content="0; url=https://docs.plasma-mobile.org/Installation.html" />

<p>This page has moved, you should be automatically redirected. If not, please click <a href="https://docs.plasma-mobile.org/Installation.html">here</a>.</p>
